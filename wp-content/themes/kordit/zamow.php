<?php get_header(); ?>
<main>
	<section id="header-service">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-xs-12">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('
								<p id="breadcrumbs">','</p>
								');
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="title-header">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h1><?php echo short_filter_wp_title( $title ); ?></h1>
						<p>Nie rozumiesz jakiegoś pytania? <script type="text/javascript">
							if (screen && screen.width > 480) {
								document.write('Najedź na pole i przeczytaj pomoc')
							}else
							{
								document.write('Kliknij na pole i przeczytaj pomoc')
							}
						</script></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<?php echo do_shortcode( '[contact-form-7 id="4701" title="Zamówienie"]' ); ?>
				</div>
				<div class="col-lg-3">
					<div class="sidebar">
						<h4 class="text-center">Masz problemy z rozbudowanym formularzem zamówienia?<br>Otwórz prosty formularz zamówienia</h4>
						<button data-toggle="modal" data-target="#form">otwórz formularz</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/wp-content/themes/quality-construction/tablica.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
//$("#form-label-1").attr("data-toggle", "tooltip");
var i = 0;
while (i < 31) {

	$("#form-label-"+i).attr("data-toggle", "tooltip");
	$("#form-label-"+i).attr("data-placement", "top");
	$("#form-label-"+i).attr("title", tab[i]);
	$("#form-label-"+i).css( 'cursor', 'help' );
	i++;
}
</script>
<div class="modal" id="form">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title text-center">Uproszczony formularz kontaktowy</h4>
				<p class="text-center">Postaj opisać się najszerzej jak potrafisz swój produkt. Będzie nam łatwiej zbudować ofertę, dostosowaną do Twoich wymagań!</p>
			</div>
			
			<!-- Modal body -->
			<div class="modal-body">
				<div class="content-popup">
					<?php echo do_shortcode('[contact-form-7 id="4730" title="Uproszczony"]'); ?>
				</div>
			</div>
			
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Zamknij formularz</button>
			</div>
			
		</div>
	</div>
</div>