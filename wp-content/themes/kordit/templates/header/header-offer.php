<?php 
if (get_the_post_thumbnail()) {
	$tlo = get_the_post_thumbnail_url();
}
if (get_field( 'grafika_tla' )) {
	$tlo = get_field( 'grafika_tla' );	
}
?>
<div class="background-grey">
	<div class="header-present oferta realizacje" style="background-image: url(<?php echo $tlo; ?>)">
		<div class="title-container">
			<h1><?php the_title(); ?></h1>
			<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
		</div>
	</div>
</div>
<div class="we-love oferta">
	<h2>oferta dla usługi "<?php the_title(); ?>" - wybierz najlepsze rozwiązanie w Lublinie!</h2>
</div>
<div class="we-love realizacje">
	<h2>Realizacja "<?php the_title(); ?>" - wybierz najlepsze rozwiązanie w Lublinie!</h2>
</div>
