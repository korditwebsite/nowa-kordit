<div class="header-present">
	<div class="title-container">
		<h1><?php the_title(); ?></h1>
		<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
	</div>
</div>