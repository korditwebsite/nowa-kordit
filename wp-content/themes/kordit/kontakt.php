<?php
/**
* Template Name: Kontakt
*/
?>
<?php get_header(); ?>
<main id="kontakt">
	<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
		<div class="container">
			<section id="contact">
				<div class="circles">
					<div class="row">
						<div class="col-xl-4">
							<div class="item-contact">
								<div class="circle">
									<div class="icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Capa_1" x="0px" y="0px" viewBox="0 0 512.076 512.076" style="enable-background:new 0 0 512.076 512.076;" xml:space="preserve" width="512px" height="512px"><g><g transform="translate(-1 -1)"> <g> <g> <path d="M499.639,396.039l-103.646-69.12c-13.153-8.701-30.784-5.838-40.508,6.579l-30.191,38.818 c-3.88,5.116-10.933,6.6-16.546,3.482l-5.743-3.166c-19.038-10.377-42.726-23.296-90.453-71.04s-60.672-71.45-71.049-90.453 l-3.149-5.743c-3.161-5.612-1.705-12.695,3.413-16.606l38.792-30.182c12.412-9.725,15.279-27.351,6.588-40.508l-69.12-103.646 C109.12,1.056,91.25-2.966,77.461,5.323L34.12,31.358C20.502,39.364,10.511,52.33,6.242,67.539 c-15.607,56.866-3.866,155.008,140.706,299.597c115.004,114.995,200.619,145.92,259.465,145.92 c13.543,0.058,27.033-1.704,40.107-5.239c15.212-4.264,28.18-14.256,36.181-27.878l26.061-43.315 C517.063,422.832,513.043,404.951,499.639,396.039z M494.058,427.868l-26.001,43.341c-5.745,9.832-15.072,17.061-26.027,20.173 c-52.497,14.413-144.213,2.475-283.008-136.32S8.29,124.559,22.703,72.054c3.116-10.968,10.354-20.307,20.198-26.061 l43.341-26.001c5.983-3.6,13.739-1.855,17.604,3.959l37.547,56.371l31.514,47.266c3.774,5.707,2.534,13.356-2.85,17.579 l-38.801,30.182c-11.808,9.029-15.18,25.366-7.91,38.332l3.081,5.598c10.906,20.002,24.465,44.885,73.967,94.379 c49.502,49.493,74.377,63.053,94.37,73.958l5.606,3.089c12.965,7.269,29.303,3.898,38.332-7.91l30.182-38.801 c4.224-5.381,11.87-6.62,17.579-2.85l103.637,69.12C495.918,414.126,497.663,421.886,494.058,427.868z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M291.161,86.39c80.081,0.089,144.977,64.986,145.067,145.067c0,4.713,3.82,8.533,8.533,8.533s8.533-3.82,8.533-8.533 c-0.099-89.503-72.63-162.035-162.133-162.133c-4.713,0-8.533,3.82-8.533,8.533S286.448,86.39,291.161,86.39z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M291.161,137.59c51.816,0.061,93.806,42.051,93.867,93.867c0,4.713,3.821,8.533,8.533,8.533 c4.713,0,8.533-3.82,8.533-8.533c-0.071-61.238-49.696-110.863-110.933-110.933c-4.713,0-8.533,3.82-8.533,8.533 S286.448,137.59,291.161,137.59z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M291.161,188.79c23.552,0.028,42.638,19.114,42.667,42.667c0,4.713,3.821,8.533,8.533,8.533s8.533-3.82,8.533-8.533 c-0.038-32.974-26.759-59.696-59.733-59.733c-4.713,0-8.533,3.82-8.533,8.533S286.448,188.79,291.161,188.79z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> </g> </g> </g></g> </svg> 
									</div>
								</div>
								<h4>Zadzwoń do nas</h4>
								<div class="label-contact">
									<span>
										Project managment:
									</span>
									<a href="tel:+48<?php the_sub_field( 'numer_telefonu', 'options' ); ?>"><?php the_sub_field( 'numer_telefonu', 'options' ); ?></a>
								</div>
								<div class="label-contact">
									<span>
										Sprzedaż:	
									</span>
									<a href="tel+48<?php the_sub_field( 'numer_telefonu', 'options' ); ?>"><?php the_sub_field( 'numer_telefonu', 'options' ); ?></a>
								</div>
							</div>
						</div>
						<div class="col-xl-4">
							<div class="item-contact">
								<div class="circle">
									<div class="icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 452.84 452.84" width="512px" height="512px"><g><g><path d="m449.483,190.4l.001-.001-57.824-38.335v-128.134c0-4.142-3.358-7.5-7.5-7.5h-315.49c-4.142,0-7.5,3.358-7.5,7.5v128.143l-57.814,38.326 .001,.002c-2.022,1.343-3.357,3.639-3.357,6.249v232.26c0,4.142 3.358,7.5 7.5,7.5h437.84c4.142,0 7.5-3.358 7.5-7.5v-232.26c0-2.61-1.335-4.906-3.357-6.25zm-388.313,26.229l-23.525-12.479h23.525v12.479zm-46.17-7.511l172.475,91.49-172.475,114.327v-205.817zm211.417,83.671l194.037,128.621h-388.073l194.036-128.621zm38.945,7.82l172.477-91.491v205.821l-172.477-114.33zm126.298-96.459h23.536l-23.536,12.484v-12.484zm28.794-15h-28.794v-19.09l28.794,19.09zm-43.794-157.72v193.161l-125.527,66.586-20.573-13.637c-2.511-1.665-5.776-1.665-8.287,0l-20.57,13.635-125.533-66.589v-193.156h300.49zm-315.49,157.72h-28.782l28.782-19.08v19.08z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path><path d="m226.415,213.671h59.754c4.142,0 7.5-3.358 7.5-7.5s-3.358-7.5-7.5-7.5h-59.754c-28.813,0-52.254-23.441-52.254-52.254v-2.213c0-28.813 23.441-52.254 52.254-52.254s52.254,23.441 52.254,52.254v5.533c0,6.237-5.074,11.312-11.312,11.312s-11.312-5.074-11.312-11.312v-10.512c0-17.864-14.533-32.398-32.397-32.398s-32.397,14.533-32.397,32.398c0,17.864 14.533,32.397 32.397,32.397 8.169,0 15.636-3.045 21.34-8.052 4.644,7.483 12.932,12.478 22.369,12.478 14.508,0 26.312-11.803 26.312-26.312v-5.533c0-37.084-30.17-67.254-67.254-67.254s-67.254,30.17-67.254,67.254v2.213c5.68434e-14,37.085 30.17,67.255 67.254,67.255zm-2.767-57.049c-9.593,0-17.397-7.804-17.397-17.397s7.805-17.398 17.397-17.398 17.397,7.805 17.397,17.398-7.804,17.397-17.397,17.397z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path></g></g></svg> 
									</div>
								</div>
								<h4>Napisz do nas</h4>
								<div class="label-contact">
									<span>
										Project managment:
									</span>
									<a href="mailto:<?php the_sub_field( 'mail', 'options' ); ?>"><?php the_sub_field( 'mail', 'options' ); ?></a>
								</div>
								<div class="label-contact">
									<span>
										Sprzedaż:	
									</span>
									<a href="mailto:<?php the_sub_field( 'mail_wyceny', 'options' ); ?>"><?php the_sub_field( 'mail_wyceny', 'options' ); ?></a>
								</div>
							</div>
						</div>
						<div class="col-xl-4">
							<div class="item-contact">
								<div class="circle">
									<div class="icon">
										
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px"><g><g> <path d="M30,26c3.86,0,7-3.141,7-7s-3.14-7-7-7s-7,3.141-7,7S26.14,26,30,26z M30,14c2.757,0,5,2.243,5,5s-2.243,5-5,5 s-5-2.243-5-5S27.243,14,30,14z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M29.823,54.757L45.164,32.6c5.754-7.671,4.922-20.28-1.781-26.982C39.761,1.995,34.945,0,29.823,0 s-9.938,1.995-13.56,5.617c-6.703,6.702-7.535,19.311-1.804,26.952L29.823,54.757z M17.677,7.031C20.922,3.787,25.235,2,29.823,2 s8.901,1.787,12.146,5.031c6.05,6.049,6.795,17.437,1.573,24.399L29.823,51.243L16.082,31.4 C10.882,24.468,11.628,13.08,17.677,7.031z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M42.117,43.007c-0.55-0.067-1.046,0.327-1.11,0.876s0.328,1.046,0.876,1.11C52.399,46.231,58,49.567,58,51.5 c0,2.714-10.652,6.5-28,6.5S2,54.214,2,51.5c0-1.933,5.601-5.269,16.117-6.507c0.548-0.064,0.94-0.562,0.876-1.11 c-0.065-0.549-0.561-0.945-1.11-0.876C7.354,44.247,0,47.739,0,51.5C0,55.724,10.305,60,30,60s30-4.276,30-8.5 C60,47.739,52.646,44.247,42.117,43.007z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> </g></g> </svg> 
									</div>
								</div>
								<h4>Odwiedź nas</h4>
								<div class="label-contact">
									<span>
										<?php the_sub_field( 'adres', 'options' ); ?>
									</span>
								</div>
							</div>
						</div>
						<div class="bottom-contact-container">
							<div class="row">
								<div class="col-xl-6">
									<div class="item-contact bottom-list-contact">
										<?php echo do_shortcode('[contact-form-7 id="584" title="Podstawowy formularz"]'); ?>
									</div>
								</div>
								<div class="col-xl-6">
									<div class="map">
										<iframe src="https://snazzymaps.com/embed/164438" width="100%" height="350px" style="border:none;"></iframe>
									</div>
									<span>
										<b class="d-block"><?php the_sub_field( 'nad_formularzem', 'options' ); ?></b>
									</span>
									<span>
										<?php the_sub_field( 'infomracje_dodatkowe', 'options' ); ?>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	<?php endwhile; endif; ?>
</main>
<?php get_footer(); ?>