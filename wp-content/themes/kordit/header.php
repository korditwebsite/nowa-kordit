<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="google-site-verification" content="aViWC_fdJ8s6xWort5qaWiQc5Hs0bjwLllFqna68Mjo" />
	<?php wp_head(); ?>
	<title><?php wp_title('the_title_attribute();'); ?></title>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<link href="https://fonts.googleapis.com/css?family=Orbitron:400,700|Quicksand:400,500,700&display=swap&subset=latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Play&display=swap&subset=latin-ext" rel="stylesheet">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142643098-1"></script>
	<meta property="og:title" content="<?php wp_title('the_title_attribute();'); ?>" />
	<meta name="keywords" content="Strony internetowe Lublin, Strony internetowe Częstochowa, sklepy internetowe Lublin, sklepy internetowe Częstochowa, projektowanie stron internetowych lublin, tworzenie stron internetowych Lublin, projektowanie stron internetowych Częstochowa, projektowanie stron internetowych Częstochowa, programowanie stron internetowych Lublin, programowanie stron internetowych Częstochowa, Tworzenie stron www Lublin, Tworzenie stron www Częstochowa" />
	<!-- Hotjar Tracking Code for kordit.pl -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:1447143,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-142643098-1');
	</script>
	<!-- Global site tag (gtag.js) - Google Ads: 785048018 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-785048018"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'AW-785048018');
	</script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-116345191-1');
	</script>
	
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1098169100361389');
			fbq('track', 'PageView');
		</script>
		<noscript><img alt="strony internetowe Lublin" height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=1098169100361389&ev=PageView&noscript=1"
			/></noscript>
			<link href="https://fonts.googleapis.com/css?family=Quicksand:400,700&display=swap&subset=latin-ext" rel="stylesheet">
		</head>
		<body <?php body_class( 'class-name' ); ?>>
			<div id="navi-button">
				<div class="c-navi-button" id="js-hamburger">
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
				</div>
			</div>
			<div id="wptime-plugin-preloader"></div>
			<nav class="ac" id="mainmenu">
				<?php
				wp_nav_menu([
					'menu'            => 'top',
					'theme_location'  => 'top',
					'container'       => 'div',
					'container_id'    => 'bs4navbar',
					'menu_class'      => 'navbar-nav',
					'depth'           => 2,
					'fallback_cb'     => 'bs4navwalker::fallback',
					'walker'          => new bs4navwalker(),
				]);
				?>
				<div class="oferta-navi">
					<h4>Oferta</h4>
					<ul>
						<?php
						$args = array(
							'post_type'   => "oferta",
							'post_status' => 'publish',
							'posts_per_page' => '99',
							'order' => 'ASC',
							'orderby' => 'name'
						);

						$testimonials = new WP_Query( $args );
						if( $testimonials->have_posts() ) :
							?>
							<?php $menu = 0;
							while( $testimonials->have_posts() ) :
								$testimonials->the_post();
								?>
								<li>
									<a title="<?php the_title(); ?>" href="<?php echo get_permalink( $post->ID ); ?>">
										<?php the_title(); ?>
									</a>
								</li>
								<?php
								$menu = $menu + 1;
							endwhile;
							wp_reset_postdata();
							?>
							<?php
						else :
							esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
						endif;
						?>
					</ul>
				</div>
				<div class="realizacje-navi">
					<h4>Realizacje</h4>
					<?php
					$args = array(
						'post_type'   => "realizacja",
						'post_status' => 'publish',
						'posts_per_page' => '-1',
						'order' => 'ASC',
						'orderby' => 'name'
					);

					$testimonials = new WP_Query( $args );
					if( $testimonials->have_posts() ) :
						?>
						<ul>
							<?php $menu = 0;
							while( $testimonials->have_posts() ) :
								$testimonials->the_post();
								?>
								<li>
									<a title="<?php the_title(); ?>" href="<?php echo get_permalink( $post->ID ); ?>">
										<?php the_title(); ?>
									</a>
								</li>
								<?php
								$menu = $menu + 1;
							endwhile;
							wp_reset_postdata();
							?>
						</ul>
						<?php
					else :
						esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
					endif;
					?>
				</div>
				<img class="logotype" src="/wp-content/uploads/2019/07/logo-kordit-w.png">
			</nav>
			<!-- Main navigation -->
			<header>
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-6 col-6">
							<div class="navbar-brand">
								<a href="/">
									<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
									$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
									if ( has_custom_logo() ) {
										echo '<img alt="logotyp" class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
									} else {
										echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
									} ?>
								</a>
							</div>
						</div>
						<div class="col-xl-6 col-6 flex-right">
							<div id="menu-button">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
					</div>
				</div>
			</header>
			<?php
			if (is_page_template( 'homepage.php' )) {
				
			} else if (is_singular()) {
				get_template_part( 'templates/header/header', 'offer');

			} else {
				//get_template_part( 'templates/header/header', 'page');
			}
			?>






