<?php get_header(); ?>
<main id="realizacje">
	<section id="portfolio" class="panel" data-section-name="portfolio">
		<div class="container">
			<div class="pos-button">
				<a class="wroc" href="/portfolio">Wróć do wszystkich realizacji</a>
			</div>
			<div class="inner-container">
				<h3>O projekcie</h3>
				<p><?php the_field("krotki_opis"); ?></p>
			</div>
		</div>
		<div class="grey-slider-container">
			<div id="sliderportfolio" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php $images = get_field('galeria');
					$size = 'gallery';
					$size2 = 'hero_image';
					if( $images ): ?>
						<?php $iterate = 0; foreach( $images as $image ):
						if ($iterate == 0) {
							$active = "active";
						} else {
							$active = "";
						}
						?>
						<li data-target="#sliderportfolio" data-slide-to="<?php echo $iterate ?>" class="<?php echo $active; ?>"></li>
						<?php $iterate = $iterate +1; endforeach; ?>
					<?php endif;  ?>
				</ol>
				<div class="carousel-inner">
					<?php if( $images ): $iterateimage = 0; foreach( $images as $image ):
						if ($iterateimage == 0) {
							$activei = "active";
						} else {
							$activei = "";
						}
						?>
						<div class="carousel-item <?php echo $activei; ?>">
							<img class="d-block w-100" src="<?php echo wp_get_attachment_image_url( $image['ID'], $size2 ); ?>" alt="First slide">
						</div>
						<?php $iterateimage = $iterateimage +1; endforeach; ?>
					<?php endif;  ?>
				</div>
				<a class="carousel-control-prev" href="#sliderportfolio" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#sliderportfolio" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class="container">
			<div class="solutions-used">
				<h3>Funkcjonalności projektu: </h3>
				<div class="container">
					<?php if ( have_rows( 'uzyte_rozwiazania' ) ) : ?>
						<ul>
							<?php while ( have_rows( 'uzyte_rozwiazania' ) ) : the_row(); ?> 
								<li class="<?php if( get_sub_field('bootstrap') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/responsive.png">
									<span>
										responsywność
									</span>
								</li>

								<li class="<?php if( get_sub_field('galeria') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/gallery.png">
									<span>
										galeria
									</span>
								</li>

								<li class="<?php if( get_sub_field('strefa_klienta') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/value.png">
									<span>
										strefa klienta
									</span>
								</li>

								<li class="<?php if( get_sub_field('formularz_kontaktowy') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/contact-form.png">
									<span>
										formularz kontaktowy
									</span>
								</li>

								<li class="<?php if( get_sub_field('mapa_dojazdu') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/placeholder.png">
									<span>
										mapa dojazdu
									</span>
								</li>
								<li class="<?php if( get_sub_field('dodatkowa_optymalizacja') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/09/speed-1.png">
									<span>
										<b>Dodatkowa optymalizacja strony</b>
									</span>
								</li>
								<li class="<?php if( get_sub_field('copywriting') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/09/copywriter-1.png">
									<span>
										Copywriting
									</span>
								</li>

								<li class="<?php if( get_sub_field('blog') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/blogging.png">
									<span>
										blog
									</span>
								</li>

								<li class="<?php if( get_sub_field('niestandardowy_typ_postu') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/favorites.png">
									<span>
										niestandardowy typ postu
									</span>
								</li>

								<li class="<?php if( get_sub_field('system_rezerwacji') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/calendar.png">
									<span>
										system rezerwacji
									</span>
								</li>

								<li class="<?php if( get_sub_field('platnosci') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/pay.png">
									<span>
										płatności
									</span>
								</li>

								<li class="<?php if( get_sub_field('sklep') ): echo "active"; endif; ?>">
									<img class="icon-solutions" src="/wp-content/uploads/2019/07/shopping-cart.png">
									<span>
										funkcjonalności sklepu internetowego
									</span>
								</li>

							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ( have_rows( 'mobile' ) ) : ?>
			<?php while ( have_rows( 'mobile' ) ) : the_row(); ?>
				<div class="grey-slider-container">
					<div class="container">
						<div class="row">
							<div class="col-xl-4">
								<div class="thumbnail">
									<?php $attachment_id = get_sub_field( 'mobile_1' );
									$size          = "full";
									$image         = wp_get_attachment_image_src( $attachment_id, $size );

									echo '<img src="' . $image[0] . '" />';
									?>
								</div>
							</div>
							<div class="col-xl-4">
								<div class="thumbnail">
									<?php $attachment_id = get_sub_field( 'mobile_2' );
									$size          = "full";
									$image         = wp_get_attachment_image_src( $attachment_id, $size );

									echo '<img src="' . $image[0] . '" />';
									?>
								</div>
							</div>
							<div class="col-xl-4">
								<div class="thumbnail">
									<?php $attachment_id = get_sub_field( 'mobile_3' );
									$size          = "full";
									$image         = wp_get_attachment_image_src( $attachment_id, $size );

									echo '<img src="' . $image[0] . '" />';
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
		<div class="container">
			<div class="other-page">
				<div class="row">
					<?php $number = get_field("numer"); ?>
					<div class="col-xl-12">	
						<h3 class="title text-center mt-5 mb-3">Sprawdź podobne realizacje</h3>
					</div>
					<?php
					$nazwaKategori = "realizacje";
					$cat = ( isset( $_GET['wyroznione'] ) ) ? $_GET['wyroznione'] : 1;
					$args = array(
						'post_type'   => 'realizacja',
						'post_status' => 'publish',
						'order' => 'ASC',
						'posts_per_page'=>'3',
						'tax_query' => [
							[

								'taxonomy' => 'rodzaj',
								'field' => 'term_id',
								'terms' => $number,
							]
						],
					);

					$testimonials = new WP_Query( $args ); 
		// echo "<pre>"; print_r($testimonials);
					if( $testimonials->have_posts() ) :
						?>
						<?php
						while( $testimonials->have_posts() ) :
							$testimonials->the_post();
							?>
							<div class="col-xl-4 col-md-4 col-12">
								<div class="item-portfolio">
									<?php
									if ( has_post_thumbnail()  ) {
										the_post_thumbnail( 'malyprostokat' );
									}
									?>
									<div class="hover-content">
										<a href="<?php the_permalink(); ?>"><button>Sprawdź realizacje</button></a>
										<h5><?php printf(get_the_title());  ?></h5>
									</div>
								</div>
							</div>
							<?php
						endwhile;
						wp_reset_postdata();
						?>
						<?php
					else :
						esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
					endif;
					?>
				</div>
				<div id="cta">
					<div class="container">
						<div class="row">
							<div class="col-xl-8 offset-xl-2">
								<h3 class="title">Zainteresowany/a ofertą?</h3>
								<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
									<div class="buttons my-3 pb-5">
										<a href="tel:<?php the_sub_field('numer_telefonu'); ?>">
											<button class="btn-t btn-2">
												<span>
													Zadzwoń <?php the_sub_field('numer_telefonu'); ?>
												</span>
											</span>
										</button>
									</a>
									<?php $atribute = short_filter_wp_title( $title ); ?>
									<script type="text/javascript">
										jQuery(document).ready(function(){
											$('.button-call-4').click(function(){
												$('.poleoferta').val('<?php the_title(); ?>');
											})
										})
									</script>
									<button class="open-popup wow fadeInDown button-call-4 btn-t btn-1">
										<span>
											ZAMÓW TERAZ
										</span>
									</button>
								</div>
							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</main>
<?php get_footer(); ?>