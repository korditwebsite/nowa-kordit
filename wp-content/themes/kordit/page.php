<?php get_header(); ?>
<main id="page">
	<div class="container my-4">
		<?php the_content(); ?>
	</div>
</main>
<?php get_footer(); ?>
