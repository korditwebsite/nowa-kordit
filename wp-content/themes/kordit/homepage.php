<?php
/**
* Template Name: Strona główna
*/
?>
<?php get_header(); ?>
<main id="homepage">
	<section class="slider">
		<div id="slider-theme" class="carousel slide carousel-fade" data-ride="carousel">
			<div class="carousel-inner">
				<?php  if( have_rows('slider') ): $d = 0; while ( have_rows('slider') ) : the_row(); ?>

					<?php
					if ($d <= 0) {
						$active = "active";
					} else {
						$active = " ";
					}
					$bottom = get_sub_field( 'bottom' );
					$bottomtext = 'bottom:' . $bottom . ";" ;
					?>
					<div class="carousel-item <?php echo $active; ?>">
						<?php echo wp_get_attachment_image( get_sub_field('slajd'), "kontener", "", array( "class" => "lazy, img-fluid w-100", "alt" => "Strony internetowe Lublin" ) );  ?>
						<div class="text-slider">
							<div class="bg-square"></div>
							<div class="inner-text-slider">
								<div class="bg-square"></div>
								<h2 class="mini-title wow fadeInRight"><?php the_sub_field("minitytul"); ?></h2>
								<h3 class="wow fadeInRight"><?php the_sub_field( 'tytul' );?> </h3>
								<p class="wow fadeInRight"><?php the_sub_field( 'podtytul' ); ?></p>
								<?php echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "img-abs wow fadeInLeft", "style" => $bottomtext,"alt" => "Strony internetowe Lublin" ) );  ?>
								<?php if ( have_rows( 'przycisk' ) ) : ?>
									<?php while ( have_rows( 'przycisk' ) ) : the_row();
										if ($w = 0) {
											$wtitle = '_blank';
										} else {
											$wtitle = '_self';
										}
										?>
										<a aria-label="przejdź do podstrony <?php echo $wtitle; ?>" class="wow fadeInRight" target="<?php echo $wtitle; ?>" href="<?php the_sub_field( 'link' ); ?>">
											<button class="btn-t <?php the_sub_field( 'wyglad' ); ?>">
												<span><?php the_sub_field( 'tresc' ); ?></span>
											</button>
										</a> 
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<?php $d = $d + 1; endwhile; else : endif; ?>
				</div>
				<div class="pagination">
					<a aria-label="następny slajd" href="#slider-theme" data-slide="prev">
						<img class="reverse" aria-label="następny slajd" alt="Strony internetowe Lublin" src="/wp-content/uploads/2019/07/next.svg">
					</a>
					<a href="#slider-theme" aria-label="poprzedni slajd"  alt="Strony internetowe Lublin" data-slide="next">
						<img alt="Strony internetowe Lublin" src="/wp-content/uploads/2019/07/next.svg">
					</a>
				</div>
			</div>
		</section>
		<?php if ( have_rows( 'o_nas' ) ) : ?>
			<?php while ( have_rows( 'o_nas' ) ) : the_row(); ?>
				<section class="about">
					<div class="container">
						<div class="row">
							<div class="col-xl-7 col-md-12 wow fadeInLeft">
								<h1><?php the_sub_field( 'tytul' ); ?></h1>
								<div class="desc">
									<?php the_sub_field( 'podpis' ); ?>
								</div>
							</div>
							<div class="col-xl-5 col-md-12 wow fadeInRight">
								<?php

								?>
								<?php echo wp_get_attachment_image( get_sub_field('grafika'), "big", "", array( "class" => "img-abs", "alt" => "Strony internetowe Lublin" ) );  ?>
							</div>
						</div>
					</div>
				</section>
			<?php endwhile; endif; ?>
			<section class="steps">
				<div class="container">
					<h2><?php the_field( 'tytul_kroki' ); ?></h2>
					<p class="subtitle"><?php the_field( 'opis_kroki' ); ?></p>
					<?php $p = 1; $time = 0; $timearow = 250; if ( have_rows( 'kroki' ) ) : ?>
					<?php while ( have_rows( 'kroki' ) ) : the_row(); ?>
						<?php 
						if (($p % 2) == 1) {
							$src = "/wp-content/uploads/2019/07/share-1-1.svg";
							$way = "fadeInLeft";
						} else {
							$src = "/wp-content/uploads/2019/07/share-1.svg";
							$way = "fadeInRight";
						}
						?>
						
						<div data-wow-delay="<?php echo $time ?>ms" class="inner-step wow fadeInUp">
							
							<div class="title">
								<div class="number">0<?php echo $p; ?></div>
								<span class="name"><?php the_sub_field( 'tytul' ); ?></span>
							</div>
							<div class="circle">
								<?php echo wp_get_attachment_image( get_sub_field('grafika'), "malyprostokat", "", array( "class" => "img-circle", "alt" => "Strony internetowe Lublin" ) );  ?>
								<div class="arrow">
									<img alt="Strony internetowe Lublin" data-wow-delay="<?php echo $timearow ?>ms" class="wow <?php echo $way; ?>" src="<?php echo $src; ?>"></div>
								</div>
								<div class="text">
									<?php the_sub_field( 'opis' ); ?>
								</div>
							</div>
							<?php $time = $time + 250; $timearow = $timearow + 250; $p = $p +1; endwhile; endif; ?>
						</div>
					</section>
					<section class="oferta">
						<div class="container">
							<div class="title-container">
								<h2>Oferta</h2>
							</div>
							<?php
							$nazwaKategori = get_field("nazwa_kategorii");
							$args = array(
								'post_type'   => "oferta",
								'post_status' => 'publish',
								'posts_per_page' => '99',
								'order' => 'ASC',
								'orderby' => 'name'
							);

							$testimonials = new WP_Query( $args );
							if( $testimonials->have_posts() ) :
								?>
								<div class="inner-content">
									<div class="row">
										<div class="col-xl-4 wow fadeInLeft">
											<ul class="nav">
												<?php $menu = 0;
												while( $testimonials->have_posts() ) :
													$testimonials->the_post();
													?>
													<li>
														<?php
														if ($menu == 7) {
															$activeofertamenu = "show active";
														} else {
															$activeofertamenu = " ";
														} ?>
														<a aria-label="Oferta to <?php the_title(); ?>" data-toggle="tab" title="<?php the_title(); ?>" class="<?php echo $activeofertamenu; ?>" href="#menu<?php echo $menu; ?>"><?php the_title(); ?> </a>
													</li>
													<?php
													$menu = $menu + 1;
												endwhile;
												wp_reset_postdata();
												?>
											</ul>
										</div>
										<div class="col-xl-6 offset-xl-2 wow fadeInRight">
											<div class="tab-content">
												<?php $menucontent = 0;
												while( $testimonials->have_posts() ) :
													$testimonials->the_post();

													?>
													<div id="menu<?php echo $menucontent; ?>" class="tab-pane fade <?php echo $activeoferta; ?>">
														<div class="thumbnail">
															<?php
															if ($menucontent == 6) {
																$activeoferta = "show active";
															} else {
																$activeoferta = " ";
															}
															?>
															<?php the_post_thumbnail( 'big' ); ?>
														</div>
														<h3><?php the_title(); ?> </h3>
														<div class="opis">
															<?php the_field('krotki_opis'); ?>
														</div>
														<a aria-label="Oferta to <?php the_title(); ?>" title="<?php the_title(); ?>" href="<?php echo get_permalink( $post->ID ); ?>">Czytaj więcej</a>
													</div>
													<?php
													$menucontent = $menucontent + 1;
												endwhile;
												wp_reset_postdata();
												?>
											</div>
										</div>
									</div>
								</div>
								<?php
							else :
								esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
							endif;
							?>
						</div>
					</section>
					<section id="pakiety" class="pakiety">
						<div class="container">
							<h2 class="title-section wow fadeInUp"><?php the_field("tytul_pakietow"); ?></h2>
							<div class="text-section wow fadeInUp">
								<?php the_field("opis_pakietow"); ?>
							</div>
							<div class="row">
								<?php if( have_rows('pakiet') ): $w = 1; while( have_rows('pakiet') ): the_row(); ?>
									<div class="col-xl-4 col-md-4"> 
										<div class="pakiet wow fadeInUp">
											<div class="name">
												<?php the_sub_field("nazwa"); ?>
											</div>
											<div class="list">

												<ul>
													<?php if( have_rows('co_w_zestawie') ): $x = 1; while( have_rows('co_w_zestawie') ): the_row(); ?>
														<li class="wow fadeInDown <?php if( get_sub_field('dostepnosc') ): echo "active"; endif;?>">
															<?php
															if( get_sub_field('dostepnosc') ): ?>
																<img alt="Strony internetowe Lublin" class="checked" src="/wp-content/uploads/2019/07/check.png">
															<?php else:
																echo
																'<img alt="Strony internetowe Lublin" class="checked" src="/wp-content/uploads/2019/07/png.png">';
															endif;
															the_sub_field("etykieta"); ?>
														</li>
														<?php $x = $x + 1; endwhile; endif; ?>
													</ul>
												</div>
												<tfoot>
													<div class="cena wow fadeInDown">
														<?php the_sub_field("cena"); ?>
													</div>
													<button class="open-popup wow fadeInDown button-call-<?php echo $w; ?>">ZAMÓW TERAZ
													</button>
													<small>CENA ZA 1 STRONĘ</small>
												</tfoot>
											</div>
										</div>
										<?php $w = $w + 1; endwhile; endif; ?>
									</div>
								</div>
							</section>
							<section class="portfolio">
								<div class="container-fluid">
									<div class="title-container">
										<h2>Realizacje</h2>
									</div>
									<div class="row">
										<?php
										$nazwaKategori = "realizacje";
										$tax = array( 286, 18, 214, 226, 20 );
										$cat = ( isset( $_GET['wyroznione'] ) ) ? $_GET['wyroznione'] : 1;
										$args = array( 
											'post_type'   => 'realizacja',
											'post_status' => 'publish',
											'order' => 'ASC',
											'posts_per_page'=>'6',
										);

										$testimonials = new WP_Query( $args ); 
										if( $testimonials->have_posts() ) :
											?>
											<?php
											$timeportfolio = 0;
											while( $testimonials->have_posts() ) :
												$testimonials->the_post();
												?>
												<div class="col-xl-4 col-md-6">
													<div class="row">
														<div class="item-portfolio wow fadeInUp"
														data-wow-delay="<?php echo $timeportfolio ?>ms">
														<?php
														the_post_thumbnail( 'large', array( 'title' => "strony internetowe Lublin" ) ); 
														?>
														<div class="inner-portfolio">
															<a aria-label="realizacja" title="<?php printf(get_the_title()); ?> - Kordit strony internetowe Lublin" href="<?php the_permalink(); ?>">
																<span class="link-box">
																	<img  alt="Strony internetowe Lublin" class="link" src="/wp-content/uploads/2019/07/unlink.svg">
																	<img  alt="Strony internetowe Lublin" class="click" src="/wp-content/uploads/2019/07/broken-link.svg">
																</span>
															</a>
															<h5><?php printf(get_the_title());  ?></h5>
														</div>
													</div>
												</div>
											</div>
											<?php
											$timeportfolio = $timeportfolio + 250; 
										endwhile;
										wp_reset_postdata();
										?>
										<?php
									else :
										esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
									endif;
									?>
								</div>
							</div>
						</section>
						<section class="contact">
							<div class="container">
								<div class="container">
									<div class="title-container">
										<h2>Kontakt</h2>
										<p>Wypełnij formularz kontaktowy, a oddzwonimy do Ciebie najszybciej jak to możliwe!</p>
									</div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="inner-contact">
												<?php if ( have_rows( 'kontakt', 'option' ) ) : while ( have_rows( 'kontakt', 'option' ) ) : the_row(); ?>
													<label aria-label="Nazwa firmy to <?php the_sub_field( 'nad_formularzem', 'options' ); ?>" class="basic">
														<?php the_sub_field( 'nad_formularzem', 'options' ); ?>
													</label>
													<label class="mail">
														<img alt="Strony internetowe Lublin" class="icon-contact icon-contact-mail" src="/wp-content/uploads/2019/07/email-2.svg">
														<a aria-label="napisz maila na kontakt@kordit.pl" href="mailto:<?php the_sub_field( 'mail', 'options' ); ?>">
															<?php the_sub_field( 'mail', 'options' ); ?>
														</a>
													</label>
													<label class="mail">
														<img alt="Strony internetowe Lublin" class="icon-contact icon-contact-mail" src="/wp-content/uploads/2019/07/email-2.svg">
														<a aria-label="napisz maila na kontakt@kordit.pl" href="mailto:<?php the_sub_field( 'mail_wyceny', 'options' ); ?>">
															<?php the_sub_field( 'mail_wyceny', 'options' ); ?>
														</a>
													</label>
													<label class="mobile">
														<img alt="Strony internetowe Lublin" class="icon-contact icon-contact-phone" src="/wp-content/uploads/2019/07/call.svg">
														<a aria-label="zadzwoń na numer 609 819 268" href="tel:<?php the_sub_field( 'numer_telefonu', 'options' ); ?>">
															<?php the_sub_field( 'numer_telefonu', 'options' ); ?>
														</a>
													</label>
													<label class="adress">
														<img aria-label="Adres to <?php the_sub_field( 'adres', 'options' ); ?>" alt="Strony internetowe Lublin" class="icon-contact" src="/wp-content/uploads/2019/07/placeholder-1.svg">
														<?php the_sub_field( 'adres', 'options' ); ?>
													</label>
													<label aria-label="Dodatkowe informacje: <?php the_sub_field( 'infomracje_dodatkowe', 'options' ); ?>" class="advanced">
														<?php the_sub_field( 'infomracje_dodatkowe', 'options' ); ?>
													</label>
												<?php endwhile;  endif; ?>
											</div>
										</div>
										<div class="col-xl-6 col-md-6">
											<div class="cf">
												<?php echo do_shortcode('[contact-form-7 id="584" title="Podstawowy formularz" html_id="form-id" html_class="form contact-form" html_name="Strony internetowe Lublin"]'); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</main>
					<?php get_footer(); ?>