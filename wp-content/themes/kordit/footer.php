<footer>
	<div class="container">
		<div class="inner-container-footer"></div>
		<div class="row">
			<div class="col-xl-4">
				<div class="item-footer">
					<div class="o-nas">
						<img class="logo-footer" alt="Strony internetowe Lublin" src="/wp-content/uploads/2019/01/cropped-logob.png">
						<p>Zespół pracowników Kordit to młodzi, kreatywni ludzie, którzy prężnie rozwijają się w swoim fachu. Każdy w swoim zawodzie ma doświadczenie, jednak wszyscy rozwijamy i doszkalamy się, aby być na bieżąco z nowinkami w branży. Mamy otwarte głowy pełne pomysłów, w swojej pracy dajemy z siebie więcej niż 100%!</p>
						<ul class="sm">
							<li class="instagram">
								<a rel="noreferrer" target="_blank" href="https://www.instagram.com/korditkck/">
									<img alt="Strony internetowe Lublin" src="/wp-content/uploads/2019/07/instagram.svg">
								</a>
							</li>
							<li class="facebook">
								<a rel="noreferrer" target="_blank" href="https://www.facebook.com/korditkck/">
									<img alt="Strony internetowe Lublin" src="/wp-content/uploads/2019/07/facebook.svg">
								</a>
							</li>
							<li class="youtube">
								<a rel="noreferrer" target="_blank" href="https://www.youtube.com/channel/UCX0tOojd9gsnTNySeHsNvfA">
									<img alt="Strony internetowe Lublin" src="/wp-content/uploads/2019/07/youtube-logo-1.svg">
								</a>
							</li>
							<li class="google">
								<a rel="noreferrer" target="_blank" href="https://www.google.com/search?q=kordit+lublin&oq=kordit+lublin&aqs=chrome..69i57j69i60l2j69i64.1393j0j1&sourceid=chrome&ie=UTF-8#lrd=0x4710bbb8b857db81:0x5f4de1d199a0eafa,1,,,">
									<img alt="Strony internetowe Lublin" src="/wp-content/uploads/2019/07/my-business.svg">
								</a>
							</li>
						</ul> 
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-md-4">
				<div class="item-footer">
					<div class="oferta-footer">
						<h2>Nawigacja</h2>
						<?php
						wp_nav_menu([
							'menu'            => 'top',
							'theme_location'  => 'top',
							'container'       => 'div',
							'container_id'    => 'bs4navbar',
							'menu_class'      => 'navbar-nav',
							'depth'           => 2,
							'fallback_cb'     => 'bs4navwalker::fallback',
							'walker'          => new bs4navwalker(),
						]);
						?>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-md-4">
				<div class="item-footer">
					<div class="facebook">
						<h2>Facebook</h2>
						<div id="fb-root"></div>
						<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.3&appId=314183925801016&autoLogAppEvents=1"></script>
						<div class="fb-page" data-href="https://www.facebook.com/korditkck/" data-tabs="timeline" data-width="" data-height="390px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/korditkck/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/korditkck/">Kordit - responsywne strony internetowe</a></blockquote></div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy">
			Copyright @ 2017-2019 by kordit.pl
		</div>
	</div>
</footer>
<div class="d-none">
	<div itemscope itemtype="http://schema.org/Brand">
		<span itemprop="name">Kordit- responsywne strony internetowe</span>
		<img alt="strony internetowe Lublin" itemprop="logo" src="https://kordit.pl/wp-content/uploads/2019/01/logo.png" />
	</div>
	<div itemscope itemtype="http://schema.org/Organization">
		<span itemprop="name">Kordit - responsywne strony internetowe</span>
		<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			<span itemprop="streetAddress">Kapucyńska 1-3</span>
			<span itemprop="postalCode">20-009</span>
			<span itemprop="addressLocality">Lublin, Poland</span>
		</div>
		Tel: <span itemprop="telephone">+48 609 819 268</span>,
		E-mail: <span itemprop="email">kontakt@kordit.pl</span>
	</div>
	<div itemscope itemtype="http://schema.org/Hotel">
		<h1><span itemprop="name">Kordit - strony internetowe Lublin</span></h1>
		<span itemprop="description">Firma Kordit zajmuje się tworzeniem stron internetowych opartych o system zarządzania treścią “Wordpress”. Projektujemy strony internetowe jak i dedykowane motywy graficzne, które są dostosowane do dzisiejszych standardów.</span>
		Star rating: <span itemprop="starRating" itemscope itemtype="http://schema.org/Rating">
			<meta itemprop="ratingValue" content="5">*****</span>
			Średni koszt: <span itemprop="priceRange">999zł - 2178zł</span>
		</div>
	</div>
	<div class="only-mobile">
		<div class="row">
			<div class="col-4">
				<div class="item-call">
					<a aria-label="Numer telefonu to +48609819268" href="tel:+48609819268">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512.076 512.076" style="enable-background:new 0 0 512.076 512.076;" xml:space="preserve" width="512px" height="512px"><g><g transform="translate(-1 -1)"> <g> <g> <path d="M499.639,396.039l-103.646-69.12c-13.153-8.701-30.784-5.838-40.508,6.579l-30.191,38.818 c-3.88,5.116-10.933,6.6-16.546,3.482l-5.743-3.166c-19.038-10.377-42.726-23.296-90.453-71.04s-60.672-71.45-71.049-90.453 l-3.149-5.743c-3.161-5.612-1.705-12.695,3.413-16.606l38.792-30.182c12.412-9.725,15.279-27.351,6.588-40.508l-69.12-103.646 C109.12,1.056,91.25-2.966,77.461,5.323L34.12,31.358C20.502,39.364,10.511,52.33,6.242,67.539 c-15.607,56.866-3.866,155.008,140.706,299.597c115.004,114.995,200.619,145.92,259.465,145.92 c13.543,0.058,27.033-1.704,40.107-5.239c15.212-4.264,28.18-14.256,36.181-27.878l26.061-43.315 C517.063,422.832,513.043,404.951,499.639,396.039z M494.058,427.868l-26.001,43.341c-5.745,9.832-15.072,17.061-26.027,20.173 c-52.497,14.413-144.213,2.475-283.008-136.32S8.29,124.559,22.703,72.054c3.116-10.968,10.354-20.307,20.198-26.061 l43.341-26.001c5.983-3.6,13.739-1.855,17.604,3.959l37.547,56.371l31.514,47.266c3.774,5.707,2.534,13.356-2.85,17.579 l-38.801,30.182c-11.808,9.029-15.18,25.366-7.91,38.332l3.081,5.598c10.906,20.002,24.465,44.885,73.967,94.379 c49.502,49.493,74.377,63.053,94.37,73.958l5.606,3.089c12.965,7.269,29.303,3.898,38.332-7.91l30.182-38.801 c4.224-5.381,11.87-6.62,17.579-2.85l103.637,69.12C495.918,414.126,497.663,421.886,494.058,427.868z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M291.161,86.39c80.081,0.089,144.977,64.986,145.067,145.067c0,4.713,3.82,8.533,8.533,8.533s8.533-3.82,8.533-8.533 c-0.099-89.503-72.63-162.035-162.133-162.133c-4.713,0-8.533,3.82-8.533,8.533S286.448,86.39,291.161,86.39z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M291.161,137.59c51.816,0.061,93.806,42.051,93.867,93.867c0,4.713,3.821,8.533,8.533,8.533 c4.713,0,8.533-3.82,8.533-8.533c-0.071-61.238-49.696-110.863-110.933-110.933c-4.713,0-8.533,3.82-8.533,8.533 S286.448,137.59,291.161,137.59z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> <path d="M291.161,188.79c23.552,0.028,42.638,19.114,42.667,42.667c0,4.713,3.821,8.533,8.533,8.533s8.533-3.82,8.533-8.533 c-0.038-32.974-26.759-59.696-59.733-59.733c-4.713,0-8.533,3.82-8.533,8.533S286.448,188.79,291.161,188.79z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path> </g> </g> </g></g> </svg> 
					</a>
				</div>	
			</div>
			<div class="col-4">
				<div class="item-call">
					<a aria-label="Mail telefonu to wycena@kordit.pl" href="mailto:wycena@kordit.pl">
						
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 452.84 452.84" width="512px" height="512px"><g><g><path d="m449.483,190.4l.001-.001-57.824-38.335v-128.134c0-4.142-3.358-7.5-7.5-7.5h-315.49c-4.142,0-7.5,3.358-7.5,7.5v128.143l-57.814,38.326 .001,.002c-2.022,1.343-3.357,3.639-3.357,6.249v232.26c0,4.142 3.358,7.5 7.5,7.5h437.84c4.142,0 7.5-3.358 7.5-7.5v-232.26c0-2.61-1.335-4.906-3.357-6.25zm-388.313,26.229l-23.525-12.479h23.525v12.479zm-46.17-7.511l172.475,91.49-172.475,114.327v-205.817zm211.417,83.671l194.037,128.621h-388.073l194.036-128.621zm38.945,7.82l172.477-91.491v205.821l-172.477-114.33zm126.298-96.459h23.536l-23.536,12.484v-12.484zm28.794-15h-28.794v-19.09l28.794,19.09zm-43.794-157.72v193.161l-125.527,66.586-20.573-13.637c-2.511-1.665-5.776-1.665-8.287,0l-20.57,13.635-125.533-66.589v-193.156h300.49zm-315.49,157.72h-28.782l28.782-19.08v19.08z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path><path d="m226.415,213.671h59.754c4.142,0 7.5-3.358 7.5-7.5s-3.358-7.5-7.5-7.5h-59.754c-28.813,0-52.254-23.441-52.254-52.254v-2.213c0-28.813 23.441-52.254 52.254-52.254s52.254,23.441 52.254,52.254v5.533c0,6.237-5.074,11.312-11.312,11.312s-11.312-5.074-11.312-11.312v-10.512c0-17.864-14.533-32.398-32.397-32.398s-32.397,14.533-32.397,32.398c0,17.864 14.533,32.397 32.397,32.397 8.169,0 15.636-3.045 21.34-8.052 4.644,7.483 12.932,12.478 22.369,12.478 14.508,0 26.312-11.803 26.312-26.312v-5.533c0-37.084-30.17-67.254-67.254-67.254s-67.254,30.17-67.254,67.254v2.213c5.68434e-14,37.085 30.17,67.255 67.254,67.255zm-2.767-57.049c-9.593,0-17.397-7.804-17.397-17.397s7.805-17.398 17.397-17.398 17.397,7.805 17.397,17.398-7.804,17.397-17.397,17.397z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#ffffff"></path></g></g></svg> 
					</a>
				</div>	
			</div>
			<div class="col-4">
				<div class="item-call">
					<span class="open-popup">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 434.168 434.168" style="enable-background:new 0 0 434.168 434.168;" xml:space="preserve" width="512px" height="512px" class=""><g><g>
							<path d="M332.318,230.196V34.967H40.93v364.235h134.038c9.616,0,17.483,7.867,17.483,17.483s-7.867,17.483-17.483,17.483H23.446   c-9.616,0-17.483-7.867-17.483-17.483V17.483C5.963,7.867,13.831,0,23.446,0h326.354c9.616,0,17.483,7.867,17.483,17.483v212.713   c0,9.616-7.867,17.483-17.483,17.483S332.318,239.812,332.318,230.196z M422.357,272.739c-7.285-6.411-18.357-5.828-24.768,1.457   l-95.867,106.648l-48.079-46.331c-6.993-6.702-18.066-6.411-24.768,0.583s-6.411,18.066,0.583,24.768l61.191,58.86   c3.205,3.205,7.576,4.954,12.238,4.954c0.291,0,0.291,0,0.583,0c4.662-0.291,9.324-2.331,12.238-5.828l107.814-120.052   C430.224,290.222,429.641,279.15,422.357,272.739z M268.212,101.986H110.863c-9.616,0-17.483,7.867-17.483,17.483   s7.867,17.483,17.483,17.483h157.349c9.616,0,17.483-7.867,17.483-17.483S277.828,101.986,268.212,101.986z M285.696,215.627   c0-9.616-7.867-17.483-17.483-17.483H110.863c-9.616,0-17.483,7.867-17.483,17.483c0,9.616,7.867,17.483,17.483,17.483h157.349   C277.828,233.11,285.696,225.243,285.696,215.627z M110.863,291.388c-9.616,0-17.483,7.867-17.483,17.483   c0,9.616,7.867,17.483,17.483,17.483h46.622c9.616,0,17.483-7.867,17.483-17.483c0-9.616-7.867-17.483-17.483-17.483H110.863z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
						</g></g> </svg>

					</a>
				</div>	
			</div>
		</div>
	</div>
	<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				xfbml            : true,
				version          : 'v3.3'
			});
		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/pl_PL/sdk/xfbml.customerchat.js';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<!-- Your customer chat code -->
		<div class="fb-customerchat"
		attribution=setup_tool
		page_id="1053861104752419">
	</div>
	<section class="popup" id="popup">
		<div class="cnavi-button remove-popup is-active" id="js-hamburger">
			<span class="line"></span>
			<span class="line"></span>
			<span class="line"></span>
		</div>
		<div class="inner-popup">
			<h2 class="title-popup">Formularz zamówienia</h2>
			<p class="info-popup">Gratulacje, dzieli Cię jeszcze jeden krok do bezpłatnej wyceny! Pamiętaj, informację zwrotną dostaniesz na maila, którego podasz w formularzu zamówienia!</p>
			<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
			<div class="thanks">
				<h2>Dziękujemy za przesłanie formularza</h2>
				<a href="/">Powróć do strony głównej</a>
			</div>
		</div>
	</section>
	<?php wp_footer(); ?>
	<script src="https://pixel.fasttony.es/373742393489779/" async defer></script>
</body>
</html>
