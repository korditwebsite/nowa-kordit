$( ".menu-item-105" ).mouseover(function() {
	$( ".oferta-navi" ).addClass('active');
});
$( ".menu-item-107" ).mouseover(function() {
  $( ".realizacje-navi" ).addClass('active');
});

var wow = new WOW(
{
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  }
  );
wow.init();


//dodawanie animacji do menu
$( "#menu-nawigacja-home li" ).addClass('wow fadeInDown');

var countitem = $("#menu-nawigacja-home > li").length;
var timeloop = 1;
for (var i = 0; i < countitem + 1; i++) {
	var name = "#menu-nawigacja-home > li:nth-child(" + i + ")";
	$(name).attr("data-wow-delay", timeloop + "s");
	var timeloop = timeloop + 0.25;
}

$(document).ready(function(){
  $(".c-navi-button").click(function(){
    $(this).toggleClass("is-active");
  });
});

$("#navi-button").click(function(){
  $("#mainmenu").toggleClass("active");
  $(this).toggleClass("active");
  $("main").toggleClass("blur");
  $("footer").toggleClass("blur");;

});
$("#navi-button.active").click(function(){
  $("#mainmenu").toggleClass("anime-out");
});


//Rozjaśnianie scroll slider
$(window).scroll(function(){
  $("#slider-theme").css("opacity", 1 - $(window).scrollTop() / 750);
});

//otwieranie popupu

$(".open-popup").click(function(){
  $("#popup").addClass("active");
});
$(".remove-popup").click(function(){
  $("#popup").removeClass("active");
});

$(".button-2").click(function(){
  $('a[href*="#openpopup"]').each(function() {
    $("#popup").addClass("active");
  });
});

var wpcf7Elm = document.querySelector( '.wpcf7' );


//wybieranie pakietu
document.addEventListener( 'wpcf7mailsent', function( event ) {
  $('.popup').addClass('send-success');
}, false );

$('.button-call-1').click(function(){
  $('select').val('Wybieram pakiet 1');
})
$('.button-call-2').click(function(){
  $('select').val('Wybieram pakiet 2');
})
$('.button-call-3').click(function(){
  $('select').val('Wybieram pakiet 3');
})

//remove container header
$(document).ready(function(){
  if($('body').hasClass('oferta-template-default')){
    $( ".we-love.realizacje" ).remove();
  }
});
$(document).ready(function(){
  if($('body').hasClass('realizacja-template-default')){
    $( ".we-love.oferta" ).remove();
  }
});


//sortowanie po active
$('.solutions-used ul li.active').prependTo('.solutions-used ul');