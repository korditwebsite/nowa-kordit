<?php get_header(); ?>
<main id="oferta">
	<div class="container-fluid">
		<div class="top-oferta">
			<div class="row">
				<div class="col-xl-6">
					<div class="item-oferta">
						<h3>Do kogo kierowana jest oferta?</h3>
						<div class="content-dla-kogo">
							<?php the_field( 'dla_kogo' ); ?>
						</div>
						<div class="dla-kogo">
							<div class="inner-dla-kogo">
								<div class="row">
									<div class="col-xl-6">
										<div class="inner-item-dla-kogo">
											<img class="icon" src="/wp-content/uploads/2019/07/promotion.svg">
											<h4>Więcej usług - niższa cena</h4>
											<p>Im więcej usług zamówisz, tym niższa cena całkowitego zamówienia!</p>
										</div>
									</div>
									<div class="col-xl-6">
										<div class="inner-item-dla-kogo">
											<img class="icon" src="/wp-content/uploads/2019/07/gift.svg">
											<h4>Wybierz pakiet</h4>
											<p>Nie możesz zdecydować jakie usługi wybrać? <a href="/#pakiety">Wybierz odpowiedni pakiet!</a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="row">
						<div class="thumbnail">
							<?php the_post_thumbnail( 'big' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-oferta">
			<div class="row">
				<div class="col-xl-6">
					<div class="row">
						<div class="thumbnail">
							<img src="/wp-content/uploads/2019/07/placeholder-2.jpg">
						</div>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="item-oferta">
						<div class="inner-item-zalety">
							<div class="inner-text-zalety">
								<h3>Zalety oferty</h3>
								<div class="w-100">
									<?php if ( have_rows( 'zalety' ) ) : ?>
										<ul>
											<?php while ( have_rows( 'zalety' ) ) : the_row(); ?>
												<li><?php the_sub_field( 'item' ); ?></li>
											<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
								<div class="w-100">
									<h3>Wyróżnia się</h3>
									<?php if ( have_rows( 'wyroznienie' ) ) : ?>
										<ul>
											<?php while ( have_rows( 'wyroznienie' ) ) : the_row(); ?>
												<li><?php the_sub_field( 'item' ); ?></li>
											<?php endwhile; ?>
										</ul>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<div class="inner-text-zalety">
							<?php $thecontent = get_the_content();
							if(!empty($thecontent)) : ?>
								<h3>O ofercie</h3>
								<?php the_content(); ?>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="cta">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 offset-xl-2">
					<h3 class="title">Zainteresowany/a ofertą?</h3>
					<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
						<div class="buttons my-3 pb-5">
							<a href="tel:<?php the_sub_field('numer_telefonu'); ?>">
								<button class="btn-t btn-2">
									<span>
										Zadzwoń <?php the_sub_field('numer_telefonu'); ?>
									</span>
								</span>
							</button>
						</a>
						<?php $atribute = short_filter_wp_title( $title ); ?>
						<script type="text/javascript">
							jQuery(document).ready(function(){
								$('.button-call-4').click(function(){
									$('.poleoferta').val('<?php the_title(); ?>');
								})
							})
						</script>
						<button class="open-popup wow fadeInDown button-call-4 btn-t btn-1">
							<span>
								ZAMÓW TERAZ
							</span>
						</button>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>
<section class="popup" id="popup">
	<div class="cnavi-button remove-popup is-active" id="js-hamburger">
		<span class="line"></span>
		<span class="line"></span>
		<span class="line"></span>
	</div>
	<div class="inner-popup">
		<h2 class="title-popup">Formularz zamówienia</h2>
		<p class="info-popup">Gratulacje, dzieli Cię jeszcze jeden krok do bezpłatnej wyceny! Pamiętaj, informację zwrotną dostaniesz na maila, którego podasz w formularzu zamówienia!</p>
		<?php echo do_shortcode('[contact-form-7 id="613" title="Podstawowy formularz ofertowy"]'); ?>
		<div class="thanks">
			<h2>Dziękujemy za przesłanie formularza</h2>
			<a href="/">Powróć do strony głównej</a>
		</div>
	</div>
</section>
</main>
<?php get_footer(); ?>