<?php
/**
* Template Name: Wszystkie realizacje
*/
?>
<?php get_header(); ?>
<main id="realizacje">
	<div class="container">
		<section id="portfolio" class="panel" data-section-name="portfolio">
			<div class="inner-content">
				<div class="container">
					<article class="row">
						<div class="col-xl-12">
							<ul class="filtermenu nav wow fadeInRight">
								<?php
								$terms = get_terms('rodzaj');
								$count = count($terms);
								echo '<li data-filter="all">Wszystkie</li>';
								if ( $count > 0 ){
									foreach ( $terms as $term ) {
										$termname = strtolower($term->name);
										$termname = str_replace(' ', '-', $termname);
										echo '<li data-filter="'.$termname.'">'.$term->name.'</li>';
									}
								}
								?>
							</ul>
							<div class="tab-content wow fadeInLeft">
								<div class="contain row">
									<?php
									$args = array(
										'post_type'   => "realizacja",
										'post_status' => 'publish',
										'order' => 'ASC'
									);
									$testimonials = new WP_Query( $args );
									if( $testimonials->have_posts() ) :
										$args = array( 'post_type' => 'realizacja', 'posts_per_page' => -1 );
										$loop = new WP_Query( $args );
										while ( $loop->have_posts() ) : $loop->the_post();
											$terms = get_the_terms( $post->ID, 'rodzaj' );
											if ( $terms && ! is_wp_error( $terms ) ) :
												$links = array();
												foreach ( $terms as $term ) {
													$links[] = $term->name;
												}
												$tax_links = join( " ", str_replace(' ', '-', $links));
												$tax = strtolower($tax_links);
											endif;

											echo '<div class="col-xl-4 col-md-6 col-sm-6 col-xs-12 post post-portfolio '. $tax .'">';
											
											echo '<div class="box">';
											echo '<img class="img-realizacje" src="'. wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) .'">';
											echo '<div class="box-info"><p>';
											?>
											<a aria-label="realizacja" title="<?php printf(get_the_title()); ?> - Kordit strony internetowe Lublin" href="<?php the_permalink(); ?>">
												<span class="link-box">
													<img  alt="Strony internetowe Lublin" class="link" src="/wp-content/uploads/2019/07/unlink.svg">
													<img  alt="Strony internetowe Lublin" class="click" src="/wp-content/uploads/2019/07/broken-link.svg">
												</span>
											</a>
											<?php
											echo '</p></div>';
											echo '</div>';
											
											echo '</div>';
										endwhile; ?>
										<?php
									else :
										esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
									endif;
									?>
								</div>
							</div>
						</div>
					</article>
				</div>
			</div>
		</section>
	</div>
</main>
<?php get_footer(); ?>