<?php
/**
* Template Name: Oferta
*/
?>
<?php get_header(); ?>
<main id="oferta">
	<section class="oferta-page">
		<div class="container">
			<div class="row">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
					$value = get_the_title();
					$str = iconv('UTF-8','ASCII//TRANSLIT',$value);
					$str = preg_replace("/[^a-z0-9- ]+/i", "", $str);
					$str = preg_replace('!\s+!', ' ', $str);
					$str = str_replace(" ", "-", $str);
					$stripped = strtolower($str);
					?>
					<article title="<?php the_title(); ?>" class="single-blog-post class-<?php echo $stripped; ?>" >
						<div class="post-container py-5">
							<div class="post">
								<div class="pb-3">
									<small><?php the_time('j F, Y'); ?> </small>
								</div>
								<div class="container-text">
									<div class="content-text">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</div>
					</article>
				<?php endwhile; else : ?>
				<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); endif ?></p>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>