<?php
/**
* Template Name: Oferta
*/
?>
<?php get_header(); ?>
<main id="oferta">
	<section class="oferta-page">
		<div class="container">
			<div class="row">
				<?php
				$nazwaKategori = get_field("nazwa_kategorii");
				$args = array(
					'post_type'   => "oferta",
					'post_status' => 'publish',
					'posts_per_page' => '99',
					'order' => 'ASC',
					'orderby' => 'name'
				);

				$testimonials = new WP_Query( $args );
				if( $testimonials->have_posts() ) :
					$time = 0;
					while( $testimonials->have_posts() ) :
						$testimonials->the_post();
						?>
						<div class="col-xl-4 col-md-6 col-12">
							<div class="inner-portfolio wow fadeInUp" data-wow-delay="<?php echo $time ?>ms">
								<div class="item-inner-portfolio">
									<div class="thumbnail">
										<?php the_post_thumbnail( 'big' ); ?>
									</div>
								</div>
								<div class="item-inner-portfolio">
									<h3><?php the_title(); ?></h3>
									<p>
										<?php
										$textshort = get_field('krotki_opis');
										echo wp_trim_words( $textshort, 20, '...' );
										?>
									</p>
									<div class="position-href">
										<a href="<?php echo get_permalink( $post->ID ); ?>">Czytaj więcej</a>
									</div>
								</div>
							</div>
						</div>
						<?php
						$time = $time + 250;
					endwhile;
					wp_reset_postdata();
				else :
					esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
				endif;
				?>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>